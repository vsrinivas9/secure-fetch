const constants = {
 UAT: {
     BASE_URL:"http://172.21.3.28:8301/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 PRODUCTION: {
     BASE_URL:"http://172.21.3.28:8301/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 TESTING: {
     BASE_URL:"http://172.21.3.28:8301/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 STAGING:{
     BASE_URL:"http://172.21.3.28:8302/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 DEMO:{
     BASE_URL:"http://172.21.3.28:8301/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 PREPROD:{
     BASE_URL:"http://172.21.3.28:8301/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 SANDBOX:{
     BASE_URL:"http://172.21.3.28:8301/api/v1/",
     SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
 },
 LOCAL:{
  BASE_URL:"http://http://localhost:8000/server/",
  SESSION_ID_WEB_ENDPOINT:'getSessionIdForWeb'
},
}

export default constants;