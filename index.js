import md5 from 'md5'
import axios from 'axios';
import constants from "./constants.js";
const salt = "testing path";
let environment={}


const copy =(obj)=>JSON.parse(JSON.stringify(obj))

const postRequestAsync = async (apiEndPoint, headers, body) => {
  try {
  const response = await axios.post(`${environment.BASE_URL}${apiEndPoint}`, body, {headers});
   console.log("postRequestAsync-response", response);
   if (response.status === 200 && response.data.success) {
    return [null,response.data];
   }
   return [response.data,null]
  } catch (e) {
   if (e.response && e.response.data) {
    const error = {
     data: {},
     msg: e.response.data.msg || 'Something went wrong !'
    }
    return [error,null]
   }
   if (!e.msg) {
    e.data = {}
    e.msg = 'Something went wrong !'
    return [e,null]
   }
   return [e,null]
  }
 }



const getSecuredRequestBody = async(sessionID=undefined,body={}) => {
  let payload = copy(body)
  payload.timestamp= new Date().getTime();
  payload.deviceType = 'web';
  if(sessionID){
    payload.sessionID=sessionID
  }
  const token = md5(JSON.stringify(payload) + salt);
  return {
   token: token,
   payload: payload
  };
 }


const generateSessionIdAndMakeARequest = async(apiEndPoint,headers, body)=>{
  const initPayload = await getSecuredRequestBody();
  const [err,result] = await postRequestAsync(`${environment.SESSION_ID_WEB_ENDPOINT}`, { 'Content-Type': 'application/json' },initPayload);
  if(err){
    throw err;
  }
  if (result.success&& result.data && result.data.sessionID) {
      let sessionID=result.data.sessionID
      localStorage.setItem('sessionID', sessionID);
      const _body = await getSecuredRequestBody(sessionID,body)
      const [err, data]=await postRequestAsync(apiEndPoint,headers, _body);
      if(err){
        throw err
      }
      return data
  }
  else{
    throw result
  }
}

const run = async (apiEndPoint, headers, body) => {
 let sessionID = localStorage.getItem('sessionID');
 if(sessionID){
  const _body = await getSecuredRequestBody(sessionID,body)
  const [err, data]=await postRequestAsync(apiEndPoint,headers, _body);
  if(data){
    return data
  }
 }
 return await generateSessionIdAndMakeARequest(apiEndPoint,headers, body)
 
}

const initEnvironment=async(env)=>{

  environment =constants[env]
}

export default {
 run,
 initEnvironment
}